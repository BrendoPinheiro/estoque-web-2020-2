package estoque;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EstoqueWebSteps {
	private static WebDriver driver;
	private static String resultado;
	
	@Given("estou na lista de compras")
	public void instanciarEstoque() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver");
		driver = new ChromeDriver();
		driver.get("file:///home/brendosp/git/estoque-web-2020-2/estoque/src/main/webapp/lista-compras.html");
		Thread.sleep(300);
	}

	@When("seleciono o produto $produto")
	public void informarProduto(String produto) throws InterruptedException {
		WebElement selecionarProduto = driver.findElement(By.id("listaProdutos"));
		selecionarProduto.sendKeys(produto, Keys.TAB);
		Thread.sleep(300);
	}
	
	@When("informo a quantidade $quantidade")
	public void informarQuantidade(String quantidade) throws InterruptedException {
		WebElement quantidadeProduto = driver.findElement(By.id("quantidade"));
		quantidadeProduto.sendKeys(quantidade, Keys.TAB);
		Thread.sleep(300);
	}
	
	@When("informo o valor unitário $valor reais")
	public void informarValor(String valor) throws InterruptedException {
		WebElement valorProduto = driver.findElement(By.id("valorUnitario"));
		valorProduto.sendKeys(valor, Keys.TAB);
		Thread.sleep(300);
	}
	
	@When("confirmo a compra")
	public void calcular() throws InterruptedException {
		WebElement btnCalcular = driver.findElement(By.id("calcularBtn"));
		btnCalcular.click();
		Thread.sleep(300);
	}
	

	@Then("terei de pagar $valorTotal reais")
	public void verificarSituacaoAluno(String valorTotal) throws InterruptedException {
		WebElement saida = driver.findElement(By.id("valorTotal"));
		resultado = saida.getAttribute("value");
		Thread.sleep(300);
		Assert.assertEquals(valorTotal, resultado);
	}

}
